import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
        apiKey: "AIzaSyCUJ0lVG4Ev9x8idtClDodTnTFkZRCofdU",
        authDomain: "module-5-f2fdb.firebaseapp.com",
        projectId: "module-5-f2fdb",
        storageBucket: "module-5-f2fdb.appspot.com",
        messagingSenderId: "290526239595",
        appId: "1:290526239595:web:d2a24514df27908f3c8a2e"),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Customers Orders Form';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: const CustomerInformation(),
      ),
    );
  }
}

// Create a Form widget.
class CustomerInformation extends StatefulWidget {
  const CustomerInformation({super.key});

  @override
  CustomerInformationState createState() {
    return CustomerInformationState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class CustomerInformationState extends State<CustomerInformation> {
  TextEditingController namecontroller = TextEditingController();
  TextEditingController mealcontroller = TextEditingController();
  TextEditingController locationcontroller = TextEditingController();

  Future _addCustomerInformation() {
    final name = namecontroller.text;
    final meal = mealcontroller.text;
    final location = locationcontroller.text;

    final ref = FirebaseFirestore.instance.collection('sessions').doc();

    return ref.set({
      "Customer_Name": name,
      "Customer_meal": meal,
      "Customer_location": location,
      "doc_id": ref.id
    });
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            controller: namecontroller,
            decoration: const InputDecoration(
                border: OutlineInputBorder(), hintText: 'Name'),
          ),
          TextFormField(
            controller: mealcontroller,
            decoration: const InputDecoration(
                border: OutlineInputBorder(), hintText: 'Meal'),
          ),
          TextFormField(
            controller: locationcontroller,
            decoration: const InputDecoration(
                border: OutlineInputBorder(), hintText: 'Location'),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                _addCustomerInformation();
                // Validate returns true if the form is valid, or false otherwise.
                if (_formKey.currentState!.validate()) {
                  // If the form is valid, display a snackbar. In the real world,
                  // you'd often call a server or save the information in a database.
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Processing Data')),
                  );
                }
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}
